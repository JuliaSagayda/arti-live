jQuery(function() {

    jQuery('#popupbutton').fancybox({
        'padding': 37,
        'overlayOpacity': 0.87,
        'overlayColor': '#fff',
        'transitionIn': 'none',
        'transitionOut': 'none',
        'titlePosition': 'inside',
        'centerOnScroll': true,
        'maxWidth': 400,
        'minHeight': 310

    });

    jQuery('#phone').mask('+3 (000) 000-00-00');

    jQuery("#form-feedback").submit(function(event) {

        if (jQuery('#name').val() == "")
            {
                jQuery('#bthrow_error_name').fadeIn(1000).html('Представьтесь, пожалуйста.');
            }
        else if (jQuery('#phone').val() == "")
            {
                jQuery('#bthrow_error_name').empty();
                jQuery('#bthrow_error_phone').fadeIn(1000).html('Как с Вами связаться?');
            }
			
		
        else
            {
                var postForm = {
                    'name'  : jQuery('#name').val(),
                    'phone'  : jQuery('#phone').val(),
					
					
                    
                };

                jQuery.ajax({
                    type        : 'POST',
                    url         : '/skin/frontend/smartwave/porto/js/feedback/js/feedback.php',
                    data        : postForm,
                    dataType    : 'json',
                    success     : function(data)
                        {
                            if (!data.success)
                                {
                                    if (data.errors.name)
                                        {
                                            jQuery('.throw_error').fadeIn(1000).html(data.errors.name);
                                        }
                                }
                            else
                                {
                                    jQuery('#form-feedback').fadeIn(1000).html('<p>' + data.posted + '</p>');
                                }
                        }
                });
            }

        event.preventDefault();

    });

});
