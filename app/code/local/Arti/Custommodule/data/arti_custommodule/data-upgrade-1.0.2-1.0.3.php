<?php

/**
 * @var $this Mage_Core_Model_Resource_Setup
 */

$this->setConfigData( 'currency/options/allow', 'UAH', 'default', 0 );
$this->setConfigData( 'currency/options/allow', 'USD', 'stores', 3 );
$this->setConfigData( 'currency/options/allow', 'USD,UAH', 'stores', 1 );
$this->setConfigData( 'currency/options/allow', 'USD,UAH', 'stores', 2 );

$this->setConfigData( 'currency/options/default', 'UAH', 'default', 0 );
$this->setConfigData( 'currency/options/default', 'USD', 'stores', 3 );

$this->getConnection()->exec("
insert into `directory_currency_rate` (`currency_from`, `currency_to`, `rate`) values ('UAH','USD','0.038461538462')
on DUPLICATE KEY UPDATE `rate`='0.038461538462';
");

