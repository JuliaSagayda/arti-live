<?php

class Arti_Custommodule_Model_Stock_Item extends Mage_CatalogInventory_Model_Stock_Item
{
	public function checkQuoteItemQty($qty, $summaryQty, $origQty = 0) {
		$quoteQty = 0;
		$quote = Mage::getSingleton('checkout/session')->getQuote();
		foreach ($quote->getAllItems() as $item) {
			if ( $item->getProductId() == $this->getProductId() && $item->getOrigData() ) {
				$quoteQty = $item->getQty()-$summaryQty;
				break;
			}
		}
		$availQty = $this->getQty() - $this->getMinQty() - $quoteQty;
		$result = parent::checkQuoteItemQty($qty, $summaryQty, $origQty);
		if ( $result->getMessage() == Mage::helper('cataloginventory')->__('The requested quantity for "%s" is not available.', $this->getProductName()) ) {
			$result->setMessage( Mage::helper('custommodule')->__('The requested quantity for "%s" is not available. Available quantity: %d', $this->getProductName(), $availQty) );
		}
		return $result;
	}
}