<?php

class Arti_Custommodule_Model_Layer_Filter_Decimal extends Mage_Catalog_Model_Layer_Filter_Decimal {

	protected function _renderItemLabel($range, $value)
	{
//		$from   = Mage::app()->getStore()->formatPrice(($value - 1) * $range, false);
//		$to     = Mage::app()->getStore()->formatPrice($value * $range, false);
		$from   = ($value - 1) * $range;
		$to     = $value * $range;
		return Mage::helper('catalog')->__('%s - %s', $from, $to);
	}

}