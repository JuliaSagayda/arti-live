<?php
/**
 * Created by PhpStorm.
 * User: eup
 * Date: 04.02.17
 * Time: 14:46
 */ 
class Arti_Custommodule_Model_Catalog_Config extends Mage_Catalog_Model_Config
{
    /**
     * Retrieve Attributes Used for Sort by as array
     * key = code, value = name
     *
     * @return array
     */
    public function getAttributeUsedForSortByArray()
    {
        $options = parent::getAttributeUsedForSortByArray();
        $options['is_in_stock'] = Mage::helper('catalog')->__('In Stock');

        return $options;
    }
}