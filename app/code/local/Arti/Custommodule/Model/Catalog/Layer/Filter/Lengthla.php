<?php

class Arti_Custommodule_Model_Catalog_Layer_Filter_Lengthla extends Arti_Custommodule_Model_Layer_Filter_Decimal
{

	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->_requestVar = 'length_la';
		$attr = Mage::getModel('eav/entity_attribute');
		$attr->loadByCode(Mage_Catalog_Model_Product::ENTITY, 'length_la');
		$this->setAttributeModel( $attr );
	}

//	/**
//	 * Apply sale filter to layer
//	 *
//	 * @param   Zend_Controller_Request_Abstract $request
//	 * @param   Mage_Core_Block_Abstract $filterBlock
//	 * @return  Mage_Catalog_Model_Layer_Filter_Sale
//	 */
//	public function apply(Zend_Controller_Request_Abstract $request, $filterBlock)
//	{
//		return $this;
//		parent::apply($request, $filterBlock);
//
//		/**
//		 * Filter must be string: $index, $range
//		 */
//		$filter = $request->getParam($this->getRequestVar());
//		if (!$filter) {
//			return $this;
//		}
//
//		$filter = explode(',', $filter);
//		if (count($filter) != 2) {
//			return $this;
//		}
//
//		list($index, $range) = $filter;
//		if ((int)$index && (int)$range) {
//			$this->setRange((int)$range);
//
//			$this->_getResource()->applyFilterToCollection($this, $range, $index);
//			$this->getLayer()->getState()->addFilter(
//				$this->_createItem($this->_renderItemLabel($range, $index), $filter)
//			);
//
//			$this->_items = array();
//		}
//
//		return $this;
//	}

	/**
	 * Get filter name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->getAttributeModel()->getStoreLabel();
	}

	/**
	 * Get data array for building sale filter items
	 *
	 * @return array
	 */
	protected function _getItemsData()
	{
		$key = $this->_getCacheKey();

		$data = $this->getLayer()->getAggregator()->getCacheData($key);
		if ($data === null) {
			$data       = array();
			$range      = $this->getRange();
			$dbRanges   = $this->getRangeItemCounts($range);

			foreach ($dbRanges as $index => $count) {
				$data[] = array(
					'label' => (($index-1)*$range) . ' - ' . ($index*$range),//$this->_renderItemLabel($range, $index),
					'value' => $index . ',' . $range,
					'count' => $count,
				);
			}


		}
		return $data;
	}



}