<?php
/**
 * Created by PhpStorm.
 * User: eup
 * Date: 04.02.17
 * Time: 14:43
 */ 
class Arti_Custommodule_Block_Catalog_Product_List_Toolbar
    extends Mage_Catalog_Block_Product_List_Toolbar
{
    /**
     * Set collection to pager.
     *
     * @param Varien_Data_Collection $collection
     * @return Mage_Catalog_Block_Product_List_Toolbar
     */
    public function setCollection($collection)
    {
        $this->_collection = $collection;

        $this->_collection->setCurPage($this->getCurrentPage());

        // we need to set pagination only if passed value integer and more that 0
        $limit = (int)$this->getLimit();
        if ($limit) {
            $this->_collection->setPageSize($limit);
        }
        if ($this->getCurrentOrder()) {
            /** Mod start */
            if ($this->getCurrentOrder() == 'is_in_stock') {
                if (!Mage::registry('stock_joined')) {
                    $this->_collection->joinField('is_in_stock',
                        'cataloginventory/stock_item',
                        'is_in_stock',
                        'product_id=entity_id',
                        '{{table}}.stock_id=1',
                        'left'
                    );
                    Mage::register('stock_joined', true);
                }
                $this->_collection->getSelect()->order($this->getCurrentOrder() . ' ' . $this->getCurrentDirection());
            } else {
                /** Mod end */
                $this->_collection->setOrder($this->getCurrentOrder(), $this->getCurrentDirection());
                /** Mod start */
            }
            /** Mod end */
        }
        return $this;
    }
}