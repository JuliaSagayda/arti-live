<?php

class Arti_Custommodule_Block_Catalog_Layer_View extends Mage_Catalog_Block_Layer_View
{
	const LENGTHLA_FILTER_POSITION = 0;

	/**
	 * State block name
	 *
	 * @var string
	 */
	protected $_lengthlaBlockName;

	/**
	 * Initialize blocks names
	 */
	protected function _initBlocks()
	{
		parent::_initBlocks();

		$this->_lengthlaBlockName = 'custommodule/catalog_layer_filter_lengthla';
	}

	/**
	 * Prepare child blocks
	 *
	 * @return Mage_Catalog_Block_Layer_View
	 */
	protected function _prepareLayout()
	{
		$saleBlock = $this->getLayout()->createBlock($this->_lengthlaBlockName)
			->setLayer($this->getLayer())
			->init();

		$this->setChild('lengthla_filter', $saleBlock);

		return parent::_prepareLayout();
	}

	/**
	 * Get all layer filters
	 *
	 * @return array
	 */
	public function getFilters()
	{
		$filters = parent::getFilters();

		if (($saleFilter = $this->_getLengthlaFilter())) {
			// Insert sale filter to the self::LENGTHLA_FILTER_POSITION position
			$filters = array_merge(
				array_slice(
					$filters,
					0,
					self::LENGTHLA_FILTER_POSITION - 1
				),
				array($saleFilter),
				array_slice(
					$filters,
					self::LENGTHLA_FILTER_POSITION - 1,
					count($filters) - 1
				)
			);
		}

		return $filters;
	}

	/**
	 * Get sale filter block
	 *
	 * @return Mage_Catalog_Block_Layer_Filter_Sale
	 */
	protected function _getLengthlaFilter()
	{
		return $this->getChild('lengthla_filter');
	}

}