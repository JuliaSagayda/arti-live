<?php

class Arti_Custommodule_Block_Blog_News_Home_List extends Smartwave_Blog_Block_Blog
{
		public function getPosts() {
			$collection = Mage::getModel('blog/blog')->getCollection()
				->addPresentFilter()
				->addEnableFilter(Smartwave_Blog_Model_Status::STATUS_ENABLED)
				->addStoreFilter()
				->setOrder('created_time', 'desc');

			$collection->setPageSize( $this->getProductCount() );

			$this->_processCollection($collection);

			return $collection;
		}


}