<?php

class Arti_Custommodule_Block_Product_List extends Mage_Catalog_Block_Product_List
{
	public function getLoadedProductCollection()
	{
		$collection = parent::getLoadedProductCollection();
		foreach ($collection as $item) {
			$stockCollection = Mage::getModel('Rugento_Multistock_Model_Resource_Stock_Collection');
			$stockCollection
				->excludeDefaultStock()
				->joinStockItemsForProduct($item)
				->excludeDisableStock()
			;
			$item->setMultistockInfo( $stockCollection );
			$inStocks = array();
			foreach ($stockCollection as $stock) {
				if ( $stock->getQty() > 0 ) {
					$inStocks[] = $stock;
				}
			}
			$item->setInStocks( $inStocks );
		}
		return $collection;
	}

}
