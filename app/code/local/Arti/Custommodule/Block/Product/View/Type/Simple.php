<?php

class Arti_Custommodule_Block_Product_View_Type_Simple extends Mage_Catalog_Block_Product_View_Type_Simple
{

	public function getProduct()
	{
		$product = parent::getProduct();
		$stockCollection = Mage::getModel('Rugento_Multistock_Model_Resource_Stock_Collection');
		$stockCollection
			->excludeDefaultStock()
			->joinStockItemsForProduct($product)
			->excludeDisableStock()
		;
		$product->setMultistockInfo( $stockCollection );
		$inStocks = array();
		foreach ($stockCollection as $stock) {
			if ( $stock->getQty() > 0 ) {
				$inStocks[] = $stock;
			}
		}
		$product->setInStocks( $inStocks );
		return $product;
	}

}
