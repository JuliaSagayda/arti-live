<?php
class Arti_Custommodule_Helper_Megamenu_Data extends Smartwave_Megamenu_Helper_Data {

	public function getMenuContent()
	{
		$menuData = Mage::helper('megamenu')->getMenuData();
		extract($menuData);
		// --- Home Link ---
		$homeLink = $this->getHomeLink();
		// --- Blog Link ---
		$blogLink = $this->getBlogLink();
		// --- Menu Content ---
//		$menuContentArray = array();
//		foreach ($_categories as $_category) {
//			$menuContentArray[] = $_block->drawMegaMenuItem($_category,'dt');
//		}
//		if (count($menuContentArray)) {
//			$menuContent = implode("\n", $menuContentArray);
//		}
//		$menuContent = '<li class="menu-item menu-item-has-children menu-parent-item big"><a href="'.Mage::getUrl( 'shop' ).'"><span>'.$this->__('Products').'</span></a>
//<div class="nav-sublist-dropdown">
//<div class="container">
//<ul>
//'.$menuContent.'
//</ul>
//</div>
//</div>
//</li>
//';
		// --- Custom Links
		$customLinks = $_block->drawCustomLinks();
		// --- Custom Blocks
		$customBlocks = $_block->drawCustomBlock();
		// --- Result ---
		$menu = <<<HTML
$homeLink
$blogLink
$customLinks
$customBlocks
HTML;
		return $menu;
	}


}