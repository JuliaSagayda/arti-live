<?php

/**
 * @var $this Mage_Eav_Model_Entity_Setup
 */

$this->startSetup();

$attributesToUpdate = array(
	'a02463a910a4cb69e3a5',
	'class_rubber_sheet',
	'conveyer_belt_mark',
	'conveyer_belt_type',
	'diameter_sealing_cord',
	'dlina_test',
	'fabric_lining_agri',
	'fabric_lining_conveyer',
	'group_sealing_cord',
	'height_oil_seal',
	'inner_diameter_hose',
	'length_agri',
	'length_goods',
	'length_la',
	'length_li',
	'length_lp',
	'length_of_coil_brake',
	'length_sheets',
	'outside_diameter_hose',
	'sizes_sealing_cord',
	'temperature_range',
	'thickness_agri',
	'thickness_brake',
	'thickness_goods',
	'thickness_sheets',
	'wall_thickness_hose',
	'width_agri',
	'width_brake',
	'width_goods',
	'width_sheets',
	'working_temperature_cord',
);

$dataToSet = array(
	'backend_model' => null,
	'backend_type' => 'varchar',
	'frontend_input' => 'text',
	//'frontend_class' => 'validate-number', //TODO: test this first.
);

foreach ($attributesToUpdate as $attributeCode) {
	$attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode(Mage_Catalog_Model_Product::ENTITY, $attributeCode);
	if ( $attributeModel->getId() ) {
		$attribute = $this->updateAttribute(
			Mage_Catalog_Model_Product::ENTITY,
			$attributeModel->getId(),
			$dataToSet
		);

		// Move the attribute values from the decimal entity table to the varchar.
		$query = 'INSERT INTO `catalog_product_entity_varchar` (`entity_type_id`, `attribute_id`, `store_id`, `entity_id`, `value`) 
			SELECT `entity_type_id`, `attribute_id`, `store_id`, `entity_id`, IF((`value` IS NOT NULL AND `value` LIKE "%.0000"), SUBSTRING(`value`, 1, CHAR_LENGTH(`value`) - 5), `value`) AS `value` 
			FROM `catalog_product_entity_decimal`
			WHERE `entity_type_id` = 4 AND
			`attribute_id`  = ' . $attributeModel->getId();

		$this->getConnection()->query($query);

		$queryDelete = 'DELETE FROM `catalog_product_entity_decimal` 
			WHERE `entity_type_id` = 4 AND
			`attribute_id` = ' . $attributeModel->getId();

		$this->getConnection()->query($queryDelete);
	}
}

$this->endSetup();