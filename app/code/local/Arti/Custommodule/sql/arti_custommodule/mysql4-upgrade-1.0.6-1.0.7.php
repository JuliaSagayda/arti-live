<?php

/**
 * @var $this Mage_Eav_Model_Entity_Setup
 */

$this->startSetup();

$attributesToUpdate = array(
	'manufacturer',
	'length_la',
	'brand',
	'inner_diameter_hose',
	'material_oli_seal',
	'height_oil_seal',
	'material_ring',
	'section_ring',
	'applicability',
	'use',
	'material_hose',
	'min_order',
	'use_hose',
	'identity_code_hose',
	'length_li',
	'marking_belt',
	'inner_diameter_oil_seal',
	'height_seal',
	'inner_diameter_seal',
	'inner_diameter_ring',
);

$dataToSet = array(
	'used_for_sort_by' => false,
);

foreach ($attributesToUpdate as $attributeCode) {
	$attributeModel = Mage::getModel('eav/entity_attribute')
        ->loadByCode(Mage_Catalog_Model_Product::ENTITY, $attributeCode);
	if ( $attributeModel->getId() ) {
		$attribute = $this->updateAttribute(
            Mage_Catalog_Model_Product::ENTITY,
            $attributeModel->getId(),
            $dataToSet
        );
	}
}

$this->endSetup();