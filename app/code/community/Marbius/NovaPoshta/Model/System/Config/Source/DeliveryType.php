<?php
/**
 * Atwix
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 * @category    Atwix Mod
 * @package     Atwix_${Global_Module_Name}
 * @author      Atwix Core Team
 * @copyright   Copyright (c) 2012 Atwix (http://www.atwix.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Marbius_NovaPoshta_Model_System_Config_Source_DeliveryType
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 'DoorsDoors', 'label' => 'Doors-Doors'),
            array('value' => 'DoorsWarehouse', 'label' => 'Doors-Warehouse'),
            array('value' => 'WarehouseDoors', 'label' => 'Warehouse-Doors'),
            array('value' => 'WarehouseWarehouse', 'label' => 'Warehouse-Warehouse')
        );
    }

}